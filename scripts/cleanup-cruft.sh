#!/bin/bash
#
# Script cleaning up the AppStream metadata pool and cache.
# This script is run weekly by a cronjob.
#

set -e
set -o pipefail
set -u

# import the general variable set.
. "/srv/appstream.debian.org/scripts/vars"
update_git

# only run one instance of the script
LOCKFILE="$WORKSPACE_DIR/.lock"
cleanup() {
    rm -f "$LOCKFILE"
}

if ! lockfile -r8 $LOCKFILE; then
    echo "aborting AppStream metadata cleanup because $LOCKFILE has already been locked"
    exit 0
fi
trap cleanup 0

# compress all existing logfiles
find "$WORKSPACE_DIR/public/logs/" -name '*.log' -exec gzip {} \;

# start logging
logdir="$WORKSPACE_DIR/public/logs/`date "+%Y/%m"`"
mkdir -p $logdir
NOW=`date "+%d_%H%M"`
LOGFILE="$logdir/${NOW}_cleanup.log"
exec >> "$LOGFILE" 2>&1

cd $WORKSPACE_DIR

# cleanup superseded data
$GENERATOR_DIR/bin/appstream-generator cleanup

# finish logging
exec > /dev/null 2>&1

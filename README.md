# Debian AppStream Generator Config

This repository contains the configuration and scripts used by Debian's instance of `appstream-generator`
on mekeel.d.o, which generates the data seen on https://appstream.debian.org/ and in our package archive.

Injected metadata that does not have a package but that we want to have in the archive (such as the `operating-system`
component for Debian itself) is also stored here.
The configuration here also permits to retroactively remove/hide components in the Debian archive (which may be necessary
in rare events).
